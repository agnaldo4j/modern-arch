## General Architecture ##

## Bad things about this app ##

Can consider this "an exercise to the reader"

- The data model is terrible:
  - Everything is non-persistent
  - URLs and usernames as primary keys
  - Feed data is 100% stored in memory and returned in its
    entirety all the time for a given URL
- Feed parsing sucks:
  - only RSS is supported, and badly
- No tests except high-level stuff for 'reader' app
- No support for SSL/TLS password sending over expected
- It is done on the run and fairly ugly
- banning doesn't cancel out existing sessions, just prevents new ones
- No CSRF protection whatsoever
- CSS is inline
