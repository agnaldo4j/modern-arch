%%% This module should be used only after the 'members' application has been
%%% started, given it depends on the presence of a given ETS table.
%%%
%%% We don't allow for passwords reset or username changes because this is just
%%% a tiny demo and I'm getting somewhat short on time.
-module(members).

-define(TABLE, members).

-export([wait/0, register/2, unregister/1, auth/2, lookup/1, ban/1, unban/1]).
-type status() :: member | undefined | banned.
-export_type([status/0]).

%% Wait for confirmation that the app is started. Shouldn't be necessary
%% to use in a release.
-spec wait() -> 'ok'.
wait() ->
    case whereis(members_sup) of
        undefined ->
            timer:sleep(100),
            wait();
        _ ->
            case ets:info(?TABLE) of
                undefined ->
                    lager:error("Members supervisor started, but not the table"),
                    error(table_died);
                _ ->
                    ok
            end
    end.

-spec register(User, Pass) -> boolean() when
    User :: iodata(),
    Pass :: iodata().
register(User, Pass) ->
    Hash = erlpass:hash(Pass),
    ets:insert_new(?TABLE, {normalize(User), Hash, member}).

-spec unregister(User :: iodata()) -> ok.
unregister(User) ->
    ets:delete(?TABLE, normalize(User)).

%% ok: authenticated
%% banned: user is banned
%% error: user or pass isn't good
-spec auth(User, Pass) -> ok | banned | error when
    User :: iodata(),
    Pass :: iodata().
auth(User, Pass) ->
    case ets:lookup(?TABLE, normalize(User)) of
        [] ->
            _ = erlpass:hash(Pass), % hide the fact we don't know the user
            error;
        [{_,_Hash,banned}] ->
            banned;
        [{_,Hash,member}] ->
            case erlpass:match(Pass, Hash) of
                true -> ok;
                false -> error
            end
    end.

-spec lookup(User :: iodata()) -> status().
lookup(User) ->
    case ets:lookup(?TABLE, normalize(User)) of
        [] -> undefined;
        [{_NormalizedUser,_,Status}] -> Status
    end.

-spec ban(User :: iodata()) -> boolean().
ban(User) ->
    ets:update_element(?TABLE, normalize(User), {3,banned}).

-spec unban(User :: iodata()) -> boolean().
unban(User) ->
    ets:update_element(?TABLE, normalize(User), {3,member}).

%%% private

%% We normalize usernames as unicode chars to make some lookups simpler across
%% apps, but one should be careful when dealing with the representation of
%% usernames.  See http://labs.spotify.com/2013/06/18/creative-usernames/ for
%% an example of this.
normalize(Chars) -> unicode:characters_to_list(Chars).

