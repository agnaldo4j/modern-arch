-module(feedstore).
-define(READS, feedstore_reads).
-export([add/1, add/2, remove/2,
         get/1, get_since/2, get_new/2, get_all_urls/1, get_all_new/1,
        subscribe/1, mark_as_read/3, last_read/2]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% General feed management %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Track a feed, unrelated to any member ever.
add(URL) ->
    feedstore_updates:track_feed(URL).

%% Get the current process to follow updates for a feed
subscribe(URL) ->
    Prop = add(URL),
    scraper:subscribe(Prop),
    Prop.

%% Get all entries for the given feed
get(URL) -> get_since(URL, {{0,0,0},{0,0,0}}).

%% Get all entries for the given feed since
%% `{{Year,Month,Day},{Hour,Minute,Seconds}}', in integers.
get_since(URL, DateTime) ->
    case feedstore_updates:get_feed(URL, DateTime) of
        {ok, Data} -> Data;
        {error,not_found} -> []
    end.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Feed <--> User matching %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Track a feed, and make `Member' follow it.
add(Member, URL) ->
    add(URL),
    mark_as_read(Member, URL, last_read(Member, URL)).

%% Have `Member' stop following the feed.
%% TODO: if no member owns the feed, stop scraping it
remove(Member, URL) ->
    ets:delete(?READS, {Member,URL}).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Member's last read entries %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Changes the last DateTime at which a given member has read
%% a given feed. This determines the return values of 'get_new' and
%% get_all_new if only new events are desired.
mark_as_read(Member, URL, DateTime={{_,_,_},{_,_,_}}) ->
    ets:insert(?READS, {{Member,URL},DateTime}).

%% Returns the last-read value for a given feed.
last_read(Member, URL) ->
    case ets:lookup(?READS, {Member,URL}) of
        [] -> {{0,0,0},{0,0,0}}; % never read
        [{_K, Since}] -> Since
    end.

%% Get new entries of the feed at `URL' for `Member'
get_new(Member, URL) ->
    Since = last_read(Member, URL),
    get_since(URL, Since).

%% Get all feeds for a member
get_all_urls(Member) ->
    Spec = [{ {{Member, '$1'}, '_'},
              [],
              ['$1']
            }],
    ets:select(?READS, Spec).

%% Get all new entries of all feeds to which `Member' subscribes
get_all_new(Member) ->
    Spec = [{ {{Member, '$1'}, '$2'},
              [],
              [{{'$1','$2'}}]
            }],
    case ets:select(?READS, Spec) of
        [] -> [];
        List ->
            [{URL, get_since(URL, Since)} || {URL,Since} <- List]
    end.
