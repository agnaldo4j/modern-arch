-module(feedstore_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(CHILD(Id, Mod, Type, Args), {Id, {Mod, start_link, Args},
                                     permanent, 5000, Type, [Mod]}).

%%%===================================================================
%%% API functions
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the supervisor
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a supervisor is started using supervisor:start_link/[2,3],
%% this function is called by the new process to find out about
%% restart strategy, maximum restart frequency and child
%% specifications.
%%
%% @spec init(Args) -> {ok, {SupFlags, [ChildSpec]}} |
%%                     ignore |
%%                     {error, Reason}
%% @end
%%--------------------------------------------------------------------
init([]) ->
    %% The feedstore_feeds table is the database archive of all feed
    %% data, no matter who it belongs to, and is managed exclusively
    %% by feedstore_update.
    ets:new(feedstore_feeds, [named_table, public, ordered_set, {read_concurrency,true}]),
    %% feedstore_reads is the table that matches up 'last read' date
    %% per member id and implicitly handles subscriptions to specific
    %% feeds.
    ets:new(feedstore_reads, [named_table, public, set, {read_concurrency,true}]),
    {ok, {{one_for_one, 5, 10},
           [?CHILD(feed_updater, feedstore_updates, worker, [])]}}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
