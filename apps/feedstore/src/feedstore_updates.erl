-module(feedstore_updates).
-include_lib("stdlib/include/ms_transform.hrl").
-define(FEEDS, feedstore_feeds).
-define(PROP_TO_FEED, feedstore_proptofeed).

-behaviour(gen_server).

%% API
-export([start_link/0, track_feed/1, get_feed/2]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-record(state, {}).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

-spec track_feed(URI::iodata()) -> PropertyName::term().
track_feed(URI) ->
    %% Track the new feed as an insert. We currently know nothing
    %% of its state.
    ets:insert_new(?FEEDS, {URI, []}),
    %% for high scalability purposes, we could track feeds in a
    %% split manner using a hash of the URI to pick one of N trackers.
    %% For now we use just one.
    %% This call has the server adding itself as a gproc property,
    %% starts a scraper, returns the subscription name
    gen_server:call(?MODULE, {scrape_follow, URI}).

get_feed(URI, {Date,Time}) ->
    Spec = [{ {{URI,'$1'},'$2'},
              [{'>','$1', {{{Date},{Time}}} }], % match spec tuple escaping :(
              [['$2']]
            }],
    case ets:select(?FEEDS, Spec) of
        [] -> {error, not_found};
        List -> {ok, lists:append(List)}
    end.

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
init([]) ->
    ets:new(?PROP_TO_FEED, [named_table, protected, set]),
    {ok, #state{}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @spec handle_call(Request, From, State) ->
%%                                   {reply, Reply, State} |
%%                                   {reply, Reply, State, Timeout} |
%%                                   {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, Reply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_call({scrape_follow,URI}, _From, State=#state{}) ->
    {ok, Prop} = scraper:scrape(URI, [{subscribe,true}]),
    case ets:lookup(?PROP_TO_FEED, Prop) of
        [{Prop, URI}] -> ok;
        [] -> ets:insert(?PROP_TO_FEED, {Prop, URI})
    end,
    {reply, Prop, State};

handle_call(_Request, _From, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @spec handle_cast(Msg, State) -> {noreply, State} |
%%                                  {noreply, State, Timeout} |
%%                                  {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_cast(_Msg, State) ->
    lager:warning("unexpected msg: ~p",[_Msg]),
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_info({feed_update, Prop, Data}, State) ->
    lager:debug("got feed updates"),
    case ets:lookup(?PROP_TO_FEED, Prop) of
        [] -> % weird state!
            lager:warning("Prop unknown: ~p", [Prop]);
        [{Prop,URI}] ->
            update(URI, Data)
    end,
    {noreply, State};
handle_info(_Info, State) ->
    lager:warning("unexpected info: ~p",[_Info]),
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
%% unsafe and inefficient way to track updates. this is a demo and
%% I'm short on time!
update(URI, Data) ->
    %% Each feed entry has its own record, indexed by the URI and
    %% the pubDate value. This allows for table scans to efficiently
    %% get last read values.
    ets:insert(?FEEDS,
               [begin
                    DateTime = proplists:get_value("pubDate", Entry),
                    {{URI,DateTime}, Entry}
                end || Entry <- Data]).
