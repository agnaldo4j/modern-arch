-module(scraper).
-export([scrape/1, scrape/2, subscribe/1, unsubscribe/1]).
-define(name(N), {n,l,N}).
-define(gproc(Name), {via, gproc, Name}).
-define(prop(Name), {p,l,Name}).

scrape(URI) -> scrape(URI, []).

scrape(URI, Opts) ->
    case sanitize_uri(URI) of
        {ok, Feed} ->
            case proplists:get_value(subscribe, Opts) of
                undefined -> ok;
                _ -> subscribe(?name({scraper, Feed}))
            end,
            maybe_start_scraper(Feed);
        {error, Reason} ->
            {error, Reason}
    end.

subscribe(Name=?name({scraper,_Feed})) ->
    %% We catch the registration because it's possible someone
    %% subscribes many times to the same feed and that fails.
    catch gproc:reg(?prop(Name)).

unsubscribe(Name=?name({scraper,_Feed})) ->
    %% We catch the deregistration because it's possible someone
    %% unsubscribes many times to the same feed and that fails.
    catch gproc:unreg(?prop(Name)).

maybe_start_scraper(Feed) ->
    Name = ?name({scraper, Feed}),
    case gproc:whereis_name(Name) of % catch duplicates
        Pid when is_pid(Pid) ->
            {ok, Name};
        undefined ->
            {ok,_Pid} = scraper_sup:start_scraper({?gproc(Name), Name, Feed}),
            {ok, Name}
    end.

%% placeholder for real work
sanitize_uri([_|_]=String) -> {ok, String};
sanitize_uri(Bin) when is_binary(Bin) -> {ok, binary_to_list(Bin)};
sanitize_uri(Term) -> {error, {invalid_uri, Term}}.
