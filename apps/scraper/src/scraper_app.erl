-module(scraper_app).
-export([start/2, stop/1]).

start(normal, _Args) ->
    scraper_sup:start_link().

stop(_Reason) ->
    ok.
