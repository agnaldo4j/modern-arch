{% extends "base.tpl" %}

{% block title %}Prototype Erlang Reader: {{ feed }}{% endblock %}

{% block nav %}
<nav>
    <ul>
        <li>Logged in as {{ username }}</li>
        <li><a href="{{ url.base }}signout">Log Out</a></li>
        <li><a href="{{ url.base }}feed">All subscribed feeds</a></li>
    </ul>
</nav>
{% endblock %}

{% block content %}
{% for entry in entries %}
<div class="feed {% if entry.new %}unread{% endif %}">
    <h3><a href="{{ entry.link }}">{{ entry.title }}</a></h3>
    <p class="date">{{ entry.pubDate }}</p>
    <form class="markread" action="{{ url.base }}feed/{{escape}}/read/{{entry.id}}" method="post">
        <input type="submit" value="mark as latest read" />
    </form>
</div>
{% endfor %}

{% endblock %}

