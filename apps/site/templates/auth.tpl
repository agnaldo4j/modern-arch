{% extends "base.tpl" %}

{% block title %}Log in{% endblock %}

{% block content %}
<h2>Log In</h2>

<p>Do note that this site doesn't support HTTPS (SSL/TLS) and you should be
   using a very dumb password for this, even if we bcrypt server-side.</p>

{% if error %}
    <div class="error">
        <p>Log in error: {{error}}</p>
    </div>
{% endif %}
<form action="{{ url.base }}auth" method="post">
{% autoescape on %}
    <label for="username">Username:</label>
    {% if username %}
        <input type="text" name="username" value="{{username}}" />
    {% else %}
        <input type="text" name="username" />
    {% endif %}
    <label for="password">Password:</label>
    <input type="password" name="password" />
    <input type="submit" value="LOG ME IN" />
{% endautoescape %}
</form>

{% endblock %}
