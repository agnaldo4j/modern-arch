-module(site_index).
-export([init/3, handle/2, terminate/3]).

init({_Any, http}, Req1, []) ->
    {SessionId, Req2} = cowboy_req:cookie(<<"session">>, Req1),
    {ok, Req2, SessionId}.

%% Unlogged
handle(Req1, undefined) ->
    {ok, URLs} = application:get_env(site, url),
    {ok, IoList} = index_tpl:render([{url,URLs}]),
    {ok, Req2} = cowboy_req:reply(
        200,
        [{<<"content-type">>, <<"text/html">>}],
        IoList,
        Req1
    ),
    {ok, Req2, undefined};
%% Possibly logged in
handle(Req1, SessionId) ->
    case reader:resume(SessionId) of
        {error, not_found} -> % not actually logged
            handle(Req1, undefined);
        {ok, _Name} -> % logged!
            %% Move to the /feed URL
            {ok, URLs} = application:get_env(site, url),
            URL = iolist_to_binary([proplists:get_value(base,URLs), "feed/"]),
            {ok, Req2} = cowboy_req:reply(
                303,
                [{<<"Location">>, URL}],
                <<"Logged in, redirecting to feeds page">>,
                Req1
            ),
            {ok, Req2, SessionId}
    end.

terminate(_Reason, _Req, _State) ->
    ok.
