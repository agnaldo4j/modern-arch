-module(site_auth).
-export([init/3, handle/2, terminate/3]).
%% called with apply/3
-export([login/2, subscribe/2]).

init({_Any, http}, Req1, [AuthType]) ->
    {ok, Req1, [AuthType]}.

handle(Req1, [unlog]) ->
    %% We don't bother with cookies, we just make the id invalid
    %% everywhere.
    {SessionId, Req2} = cowboy_req:cookie(<<"session">>, Req1),
    reader:invalidate(SessionId),
    send_to_home(Req2);
handle(Req1, [AuthType]) ->
    {SessionId, Req2} = cowboy_req:cookie(<<"session">>, Req1),
    case reader:resume(SessionId) of
        {error, not_found} -> % not actually logged
            {Method, Req3} = cowboy_req:method(Req2),
            %% dispatch based on method and AuthType, as passed
            %% by the router rules in site.erl as an optional
            %% 3rd argument.
            apply(?MODULE, AuthType, [Req3, Method]);
        {ok, _Name} -> % logged!
            send_to_feed(Req2)
    end.

login(Req1, <<"GET">>) ->
    {ok, Req2} = serve_page(auth_tpl, [], Req1),
    {ok, Req2, undefined};
login(Req1, <<"POST">>) ->
    {ok, Props, Req2} = cowboy_req:body_qs(Req1),
    Name = proplists:get_value(<<"username">>, Props, <<"">>),
    Pass = proplists:get_value(<<"password">>, Props, <<"">>),
    try
        iolist_size(Name) > 0 orelse throw([{error,"incomplete form"}]),
        iolist_size(Pass) > 0 orelse throw([{error,"incomplete form"},
                                            {username, Name}]),
        case reader:connect(Name, Pass) of
            {error, Reason} ->
                throw([{error, Reason},{username,Name}]);
            {ok, SessionId} ->
                setup_session(SessionId, Req2)
        end
    catch
        throw:Vars ->
            {ok, Req3} = serve_page(auth_tpl, Vars, Req1),
            {ok, Req3, undefined}
    end.

subscribe(Req1, <<"GET">>) ->
    {ok, Req2} = serve_page(subscribe_tpl, [], Req1),
    {ok, Req2, undefined};
subscribe(Req1, <<"POST">>) ->
    {ok, Props, Req2} = cowboy_req:body_qs(Req1),
    Name = proplists:get_value(<<"username">>, Props, <<"">>),
    Pass = proplists:get_value(<<"password">>, Props, <<"">>),
    try
        iolist_size(Name) =:= 0 andalso throw([{error, "empty username"}]),
        iolist_size(Pass) =:= 0 andalso throw([{error, "empty password"},
                                               {username, Name}]),
        case reader:account_create(Name, Pass) of
            {error, username_exists} -> throw([{error, "username exists"}]);
            ok -> ok
        end,
        case reader:connect(Name, Pass) of
            {error, Reason} ->
                throw([{error,Reason}, {username,Name}]);
            {ok, SessionId} ->
                setup_session(SessionId, Req2)
        end
    catch
        throw:Vars ->
            {ok, Req3} = serve_page(subscribe_tpl, Vars, Req2),
            {ok, Req3, undefined}
    end.

setup_session(SessionId, Req1) ->
    Req2 = cowboy_req:set_resp_cookie(<<"session">>, SessionId, [], Req1),
    send_to_feed(Req2).

serve_page(Template, Vars, Req1) ->
    {ok, URLs} = application:get_env(site, url),
    {ok, IoList} = Template:render([{url,URLs}|Vars]),
    cowboy_req:reply(
        200,
        [{<<"content-type">>, <<"text/html">>}],
        IoList,
        Req1
    ).

send_to_feed(Req) ->
    redirect_rel(<<"feed/">>,
                 <<"Logged in, redirecting to feeds page">>,
                 Req).

send_to_home(Req) ->
    redirect_rel(<<"">>, <<"Logging out.">>, Req).

redirect_rel(Path, Msg, Req) ->
    {ok, URLs} = application:get_env(site, url),
    URL = iolist_to_binary([proplists:get_value(base,URLs), Path]),
    redirect(URL, Msg, Req).

redirect(URL, Msg, Req1) ->
    {ok, Req2} = cowboy_req:reply(303, [{<<"Location">>, URL}], Msg, Req1),
    {ok, Req2, undefined}.

terminate(_Reason, _Req, _State) ->
    ok.
