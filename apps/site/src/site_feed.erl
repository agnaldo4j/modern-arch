-module(site_feed).
-export([init/3, handle/2, terminate/3]).
-export([index/3, single/3, read/3]).

init({_Any, http}, Req1, [FeedType]) ->
    {ok, Req1, [FeedType]}.

handle(Req1, [FeedType]) ->
    {SessionId, Req2} = cowboy_req:cookie(<<"session">>, Req1),
    case reader:resume(SessionId) of
        {error, not_found} -> % not actually logged
            send_to_home(Req2);
        {ok, Name} -> % logged!
            {Method, Req3} = cowboy_req:method(Req2),
            %% dispatch based on method and AuthType, as passed
            %% by the router rules in site.erl as an optional
            %% 3rd argument.
            apply(?MODULE, FeedType, [Req3, Method, Name])
    end.

index(Req1, <<"GET">>, Name) ->
    {ok, _Status, URLs} = reader:account_view(Name),
    Vars = case cowboy_req:qs_val(<<"err">>, Req1) of
        {<<"no-url">>, _} -> [{error,"no URL entered"}];
        _ -> []
    end,
    Feeds = [[{new, length(reader:feed_new(Name, URL))},
              {url, URL}, {escaped, escape(URL)}] || URL <- URLs],
    {ok, Req2} = serve_page(feeds_tpl,
                            [{feeds,Feeds}, {username,Name} | Vars],
                            Req1),
    {ok, Req2, Name};
index(Req1, <<"POST">>, Name) ->
    {ok, Props, Req2} = cowboy_req:body_qs(Req1),
    case proplists:get_value(<<"url">>, Props, <<"">>) of
        <<"">> ->
            redirect_rel(<<"feed?err=no-url">>, 303,
                         <<"Feed submitted">>, Req2);
        Feed ->
            reader:feed_follow(Name, Feed),
            added_feed(Req2)
    end.

single(Req1, _, Name) ->
    {Feed, Req2} = cowboy_req:binding(feed, Req1),
    AllEntries = lists:reverse(reader:feed_get(Feed)),
    New = [lists:keyfind("guid",1,Entry) || Entry <- reader:feed_new(Name, Feed)],
    Entries =
    [[{"id", escape(proplists:get_value("pubDate",Entry))},
      {"new",lists:member(lists:keyfind("guid",1,Entry), New)}|Entry]
     || Entry <- AllEntries],
    {ok, Req3} = serve_page(feed_tpl,
                            [{feed,Feed}, {escape,escape(Feed)},
                             {entries,Entries}, {username,Name}],
                            Req2),
    {ok, Req3, Name}.

read(Req1, _, Name) ->
    {Feed, Req2} = cowboy_req:binding(feed, Req1),
    {Stamp, Req3} = cowboy_req:binding(stamp, Req2),
    DateTime = to_calendar(Stamp),
    reader:mark_as_read(Name, Feed, DateTime),
    redirect_rel(iolist_to_binary([<<"feed/">>,escape(Feed)]),
                 303, <<"Feed submitted">>, Req3).

serve_page(Template, Vars, Req1) ->
    {ok, URLs} = application:get_env(site, url),
    {ok, IoList} = Template:render([{url,URLs}|Vars]),
    cowboy_req:reply(
        200,
        [{<<"content-type">>, <<"text/html">>}],
        IoList,
        Req1
    ).

send_to_home(Req) ->
    redirect_rel(<<"">>, 303, <<"Not logged in">>, Req).

added_feed(Req) ->
    redirect_rel(<<"feed">>, 303, <<"Feed added">>, Req).

redirect_rel(Path, Code, Msg, Req) ->
    {ok, URLs} = application:get_env(site, url),
    URL = iolist_to_binary([proplists:get_value(base,URLs), Path]),
    redirect(URL, Code, Msg, Req).

redirect(URL, Code, Msg, Req1) ->
    {ok, Req2} = cowboy_req:reply(Code, [{<<"Location">>, URL}], Msg, Req1),
    {ok, Req2, undefined}.

terminate(_Reason, _Req, _State) ->
    ok.

escape(Text) -> edoc_lib:escape_uri(unicode:characters_to_list(Text)).

%% We're using DH's date module to parse. Sadly, it doesn't support
%% timezones or offset, so we'll pretend they don't exist because this demo isn't
%% worth hacking in support for that
to_calendar(Bin) when is_binary(Bin) -> to_calendar(binary_to_list(Bin));
to_calendar(DateStr) ->
    case dh_date:parse(DateStr) of
        {error, bad_date} ->
            Str = re:replace(DateStr,
                             " (?:[A-Z]{3}|\\+[0-9]+)$",
                             "",
                             [{return, list}]),
            case dh_date:parse(Str) of
                {error, bad_date} -> error({bad_date, DateStr});
                DateTime -> DateTime
            end;
        DateTime ->
            DateTime
    end.
