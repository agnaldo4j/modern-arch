-module(site).
-export([start/1, stop/1]).

start(Port) ->
    %% On the index, check for a session. If found, redirect to feed/
    %% If not found, stay on the index, and offer links to auth/ or join/.
    %% In feed/ page, if clicking on a particular feed, move to /feed/:feed.
    %% Submit read feeds from a given feed by mashing in the guid.
    Routes = cowboy_router:compile([
        {'_',
         [{"/", site_index, []},
          {"/auth", site_auth, [login]},
          {"/subscribe", site_auth, [subscribe]},
          {"/signout", site_auth, [unlog]},
          {"/feed", site_feed, [index]},
          {"/feed/:feed", site_feed, [single]},
          {"/feed/:feed/read/:stamp", site_feed, [read]}
         ]}
    ]),
    cowboy:start_http(rss_listener, 100,
        [{port, Port}],
        [{env, [{dispatch, Routes}]}]),
    rss_listener.

stop(Ref) ->
    cowboy:stop_listener(Ref).
