%%% This module just remains alive for the duration
%%% of a given session and nothing else.
-module(reader_session).

-behaviour(gen_server).

%% API
-export([start_link/2, ping/1, stop/1]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

%%%===================================================================
%%% API
%%%===================================================================

start_link(Name, Id) ->
    gen_server:start_link(?MODULE, {Name,Id}, []).

%% keep the session alive, reset counter
ping(Pid) ->
    Pid ! ping.

stop(Pid) ->
    gen_server:call(Pid, stop),
    ok.

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

init({Name,Id}) ->
    %% Debug in the pdict
    put(name, Name),
    put(id, Id),
    {ok, nostate, timeout()}.

handle_call(stop, _From, State) ->
    {stop, {shutdown, invalidated}, ok, State};
handle_call(_Request, _From, State) ->
    {noreply, State, timeout()}.

handle_cast(_Msg, State) ->
    {noreply, State, timeout()}.

handle_info(timeout, State) ->
    {stop, {shutdown, timeout}, State};
handle_info(_Info, State) ->
    {noreply, State, timeout()}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
timeout() ->
    {ok, Val} = application:get_env(reader, session_timeout),
    Val.
