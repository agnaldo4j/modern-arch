-module(reader_manager).
-define(TABLE, reader_sessions).
-behaviour(gen_server).
%% API
-export([start_link/0]).
-export([find_session_by_name/1, find_session_by_id/1,
         start_session/1]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-record(state, {to_boot=[], monitors=gb_trees:empty()}).

%%%===================================================================
%%% API
%%%===================================================================

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

start_session(Name) ->
    Id = generate_id(),
    case reader_session_sup:start_session(Name,Id) of
        {ok, Pid} ->
            case ets:insert_new(?TABLE, [{Id, Name, Pid}, {Name, Id, Pid}]) of
                true ->
                    gen_server:cast(?MODULE, {started, Pid, Name, Id}),
                    {ok, Id};
                false ->
                    %% oh boy. If the function returns 'false', it means
                    %% we started after the older session, but before we
                    %% actually got the server to delete the entries. Let's
                    %% retry, but killing the existing session first.
                    exit(Pid, shutdown_race),
                    start_session(Name)
            end;
        _ -> % already exists
            %% this right here has a small chance of a race condition causing a
            %% non-found entry. LET'S LOOP AND GO THE NON-DETERMINISTIC WAY!
            %% THAT'S WHAT WE GET FOR USING SHARED DATA THROUGH ETS!
            case find_session_by_name(Name) of
                {error, not_found} -> % ooh race!
                    start_session(Name);
                {ok, Id} ->
                    {ok, Id}
            end
    end.

find_session_by_name(Name) ->
    case ets:lookup(?TABLE, Name) of
        [] -> {error, not_found};
        [{Name, Id, _Pid}] -> {ok, Id}
    end.

find_session_by_id(Id) ->
    case ets:lookup(?TABLE, Id) of
        [] -> {error, not_found};
        [{Id, Name, Pid}] -> {ok, Name, Pid}
    end.

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

init([]) ->
    %% Load databases. Here we only have temporary data in ETS
    %% so we warn and create blank tables.
    lager:warning("Application 'reader' uses ETS as a backend for "
                  "session storage and thus affords no persistence."),
    ets:new(?TABLE, [public, named_table, set]),
    self() ! boot_sessions, % do an async start if you have stuff to boot
    {ok, #state{}}.


handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

handle_cast({started, Pid, Name, Id}, State = #state{monitors=T}) ->
    Ref = erlang:monitor(process, Pid),
    {noreply, State#state{monitors=gb_trees:insert(Ref, {Name,Id}, T)}};
handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(boot_sessions, State = #state{to_boot=Todo}) ->
    %% We don't actually async-boot things because
    %% yay ETS!
    [start_session(Name) || Name <- Todo],
    %% Alternatively, we could synchronize sessions around the
    %% network by messaging {?MODULE, Node} for all Node in nodes().
    {noreply, State};
handle_info({'DOWN', Ref, process, _Pid, _Reason}, S=#state{monitors=T}) ->
    case gb_trees:lookup(Ref, T) of
        none -> % uh weird, but ok.
            {noreply, S};
        {value,{Name,Id}} ->
            ets:delete(?TABLE, Id),
            ets:delete(?TABLE, Name),
            %% this also works, but no indexes:
            % ets:match_delete(?TABLE, {'_','_',_Pid}),
            {noreply, S#state{monitors=gb_trees:delete(Ref,T)}}
    end;
handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
generate_id() ->
    base64:encode(crypto:rand_bytes(128)).

