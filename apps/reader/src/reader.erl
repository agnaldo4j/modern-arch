-module(reader).
-export([account_create/2, account_view/1, account_modify/2,
         account_delete/1, connect/2, resume/1, invalidate/1,
         feed_follow/2, feed_unfollow/2, feed_get/1, feed_new/2, stream/1,
         mark_as_read/3, format_entry/1]).

%% Create account
-spec account_create(Name, Password) -> ok | {error, Reason} when
    Name :: iodata(),
    Password :: iodata(),
    Reason :: term().
account_create(Name, Password) ->
    case members:register(Name, Password) of
        true -> ok;
        false -> {error, username_exists}
    end.

%% View account
-spec account_view(Name) -> {ok, members:status(), [FeedURLs]}
                          | {error, unknown} when
    Name :: iodata(),
    FeedURLs :: iodata().
account_view(Name) ->
    case members:lookup(Name) of
        undefined -> {error, unknown};
        Status -> {ok, Status, feedstore:get_all_urls(Name)}
    end.

%% Modify account
-spec account_modify(Name, ban | unban) -> {error,Reason} | ok when
    Name :: iodata(),
    Reason :: unknown | {invalid_action, term()}.
account_modify(Name, Action) ->
    case {members:lookup(Name), Action} of
        {undefined, _} -> {error, unknown};
        {_, ban} -> members:ban(Name);
        {_, unban} -> members:unban(Name);
        {_, _} -> {error, {invalid_action, Action}}
    end.

%% Delete account. Make sure to ask for authentication first
%% if the caller of this isn't the admin.
-spec account_delete(Name :: iodata()) -> ok.
account_delete(Name) ->
    members:unregister(Name),
    [feedstore:remove(Name, URL) || URL <- feedstore:get_all_urls(Name)],
    ok.

%% Connect (denied when banned!)
-spec connect(Name, Pass) -> {ok, SessionId} | {error, Reason} when
    Name :: iodata(),
    Pass :: iodata(),
    SessionId :: term(),
    Reason :: banned | error.
connect(Name, Pass) ->
    case members:auth(Name, Pass) of
        ok -> % start a session or fetch an existing one
            case reader_manager:find_session_by_name(Name) of
                {ok, SessionId} ->
                    resume(SessionId),
                    {ok, SessionId};
                {error, not_found} -> reader_manager:start_session(Name)
            end;
        Term ->
            {error, Term}
    end.

%% Resume an existing session
-spec resume(SessionId :: term()) -> {ok, Name} | {error, not_found} when
    Name :: iodata().
resume(SessionId) ->
    %% Here we just repeat the managers' results, but in practice
    %% we could have sessions flushed to disk and do things like
    %% force reloads from disk, read a peer nodes, or flush old stuff.
    case reader_manager:find_session_by_id(SessionId) of
        {error, not_found} -> {error, not_found};
        {ok,Name,Pid} ->
            reader_session:ping(Pid),
            {ok,Name}
    end.

-spec invalidate(SessionId :: term()) -> ok.
invalidate(SessionId) ->
    case reader_manager:find_session_by_id(SessionId) of
        {error, not_found} -> ok;
        {ok,_Name,Pid} -> reader_session:stop(Pid)
    end.


%% Follow a feed
-spec feed_follow(Name, Feed) -> ok when
    Name :: iodata(),
    Feed :: iodata(). % url
feed_follow(Name, Feed) ->
    feedstore:add(Name, Feed),
    ok.

%% Unfollow a feed
-spec feed_unfollow(Name, Feed) -> ok when
    Name :: iodata(),
    Feed :: iodata().
feed_unfollow(Name, Feed) ->
    feedstore:remove(Name, Feed),
    ok.

%% Get all feed content
-spec feed_get(Feed::iodata()) -> term().
feed_get(Feed) ->
    format_entries(feedstore:get(Feed)).

%% Get new feed content
-spec feed_new(Member, Feed) -> [term()] when
    Member :: iodata(),
    Feed :: iodata().
feed_new(Member, Feed) ->
    format_entries(feedstore:get_new(Member, Feed)).

%% Stream updates
-spec stream(Feed :: iodata()) -> Prop::term().
stream(Feed) ->
    feedstore:subscribe(Feed).

%% Mark as read
mark_as_read(Member, URL, DateTime={{_,_,_},{_,_,_}}) ->
    feedstore:mark_as_read(Member, URL, DateTime).

format_entries([]) -> [];
format_entries([Item|Items]) -> [format_entry(Item)|format_entries(Items)].

format_entry([]) -> [];
format_entry([{"pubDate", DateTime} | Rest]) ->
    [{"pubDate", datetime_to_pubdate(DateTime)}|Rest];
format_entry([Row|Rest]) ->
    [Row | format_entry(Rest)].

%% {{2009,9,6},{16,20,0}} -> "Mon, 06 Sep 2009 16:20:00 +0000"
datetime_to_pubdate(DateTime) ->
    dh_date:format("D, d M Y H:i:s +0000", DateTime).
