-module(reader_session_sup).

-behaviour(supervisor).

%% API
-export([start_link/0, start_session/2]).

%% Supervisor callbacks
-export([init/1]).

-define(CHILD(Id, Mod, Type, Args), {Id, {Mod, start_link, Args},
                                     temporary, 5000, Type, [Mod]}).

%%%===================================================================
%%% API functions
%%%===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

start_session(Name, Id) ->
    supervisor:start_child(?MODULE, [Name,Id]).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

init([]) ->
    {ok, {{simple_one_for_one, 5, 10},
           [?CHILD(session, reader_session, worker, [])]}}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
