-module(reader_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(CHILD(Id, Mod, Type, Args), {Id, {Mod, start_link, Args},
                                     permanent, 5000, Type, [Mod]}).

%%%===================================================================
%%% API functions
%%%===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

init([]) ->
    %% Order is important. The session supervisor must be started before
    %% the session manager is started in order for the manager to attach
    %% given session with the supervisor
    {ok, {{one_for_all, 5, 10},
          [?CHILD(session_sup, reader_session_sup, supervisor, []),
           ?CHILD(session_manager, reader_manager, worker, [])]}}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
