%%% Tests may depend on feeds from fake-rss.herokuapp.com, which allows to
%%% create and boot random tests, but may take > 5s to get started the
%%% first time around. Some tests are also running for session timeouts
%%% or depend on bcrypt passwords getting hashed multiple times and
%%% can be quite slow.
-module(reader_SUITE).
-include_lib("common_test/include/ct.hrl").
-compile(export_all).
-define(TIMEOUT, 1000). % must be divisible by 2

all() -> [accounts, sessions, feeds].

init_per_suite(Config) ->
    Deps = [crypto, lager, gproc, inets, dh_date, erlsom, bcrypt, erlpass,
            sasl, scraper, feedstore, members],
    [application:load(Dep) || Dep <- Deps],
    application:load(reader),
    overload_config(),
    [ok=application:start(Dep) || Dep <- Deps],
    ok = application:start(reader),
    lager:set_loglevel(lager_console_backend, debug),
    [{deps,Deps} | Config].

overload_config() ->
    application:set_env(scraper, min_delay, 500),
    application:set_env(reader, session_timeout, ?TIMEOUT).

end_per_suite(Config) ->
    Deps = ?config(deps, Config),
    application:stop(reader),
    [application:stop(Dep) || Dep <- Deps].

init_per_testcase(_Name, Config) ->
    Name = binary_to_list(base64:encode(crypto:rand_bytes(32))),
    URL = "http://fake-rss.herokuapp.com/feed/"++http_uri:encode(Name),
    {ok,Res} = httpc:request(post, {URL++"?interval=1",[],"text/plain",""}, [], []),
    ct:pal("Res: ~p",[Res]),
    {{_,Code,_},_,_} = Res,
    true = Code > 200 andalso Code < 400,
    [{name,Name},{url,URL}|Config].

end_per_testcase(_Name, _Config) ->
    ok.

accounts(Config) ->
    Name = ?config(name, Config),
    ok = reader:account_create(Name, "abc"),
    {ok,member,[]} = reader:account_view(Name),
    true = reader:account_modify(Name, ban),
    {ok,banned,[]} = reader:account_view(Name),
    true = reader:account_modify(Name, unban),
    {ok,member,[]} = reader:account_view(Name),
    ok = reader:account_delete(Name),
    {error, unknown} = reader:account_view(Name),
    ok = reader:account_create(Name, "abc"),
    {error, username_exists} = reader:account_create(Name, "abc"),
    {error, username_exists} = reader:account_create(Name, "def").

sessions(Config) ->
    %% connecting
    Name = ?config(name, Config),
    {error, error} = reader:connect(Name, "abc"),
    ok = reader:account_create(Name, "abc"),
    reader:account_modify(Name, ban),
    {error, banned} = reader:connect(Name, "abc"),
    reader:account_modify(Name, unban),
    {error, error} = reader:connect(Name, "def"),
    %% resuming
    {error,not_found} = reader:resume("bad session"),
    {ok, Id} = reader:connect(Name, "abc"),
    {ok, Id} = reader:connect(Name, "abc"), % found
    %% Don't die on a timeout with sufficient pings/resumes in between
    timer:sleep(?TIMEOUT div 2),
    {ok,Name} = reader:resume(Id),
    timer:sleep(?TIMEOUT div 2),
    {ok,Name} = reader:resume(Id), % found
    timer:sleep(?TIMEOUT*2),
    {error,not_found} = reader:resume(Id), % timed out
    {ok,NewId} = reader:connect(Name, "abc"),
    false = NewId =:= Id.

feeds(Config) ->
    Name = ?config(name, Config),
    URL = ?config(url, Config),
    ok = reader:account_create(Name, "abc"),
    {ok, _Id} = reader:connect(Name, "abc"),
    ok = reader:feed_follow(Name,URL),
    {ok,member,[URL]} = reader:account_view(Name),
    ok = reader:feed_unfollow(Name,URL),
    {ok,member,[]} = reader:account_view(Name),
    ok = reader:feed_follow(Name,URL),
    Feed = until(fun(Res) -> Res =/= [] end, fun() -> reader:feed_get(URL) end, 100),
    Feed = reader:feed_new(Name, URL),
    ct:pal("Feed ( ~p ): ~p",[URL,Feed]),
    %% Make sure we all get iodata for everything
    check_format(Feed),
    reader:mark_as_read(Name, URL, newest_pubdate(Feed)),
    [] = reader:feed_new(Name, URL),
    %% wait for an update
    New = until(fun(Res) -> Res =/= [] end, fun() -> reader:feed_new(Name, URL) end, 100),
    true = length(New) < reader:feed_get(URL),
    reader:mark_as_read(Name, URL, {{0,0,0},{0,0,0}}),
    true = reader:feed_new(Name, URL) =:= reader:feed_get(URL),
    ok = reader:feed_unfollow(Name, URL),
    %% A feed you don't follow is never read!
    {ok,member,[]} = reader:account_view(Name),
    true = reader:feed_new(Name, URL) =:= reader:feed_get(URL).

check_format(Feed) ->
    IsIoData = fun(Entry) ->
        [{"description", [_|_]},
         {"guid", [_|_]},
         {"link", [_|_]},
         {"pubDate", [_|_]},
         {"title", [_|_]}] = lists:sort(Entry)
    end,
    [IsIoData(Entry) || Entry <- Feed].

until(_,Fun,0) -> error({too_long,Fun()});
until(Prep, Fun, N) ->
    Res = Fun(),
    case Prep(Res) of
        true -> Res;
        false -> timer:sleep(50), until(Prep, Fun, N-1)
    end.

%% Gotta use the pubdate from the feed. Otherwise we have unsafe
%% clock skew issues possible!
newest_pubdate(Feed) ->
    Dates=[to_calendar(proplists:get_value("pubDate", Item)) || Item <- Feed],
    hd(lists:reverse(lists:sort(Dates))).

%% We're using DH's date module to parse. Sadly, it doesn't support
%% timezones or offset, so we'll pretend they don't exist because this demo isn't
%% worth hacking in support for that
to_calendar(DateStr) ->
    case dh_date:parse(DateStr) of
        {error, bad_date} ->
            Str = re:replace(DateStr,
                             " (?:[A-Z]{3}|\\+[0-9]+)$",
                             "",
                             [{return, list}]),
            case dh_date:parse(Str) of
                {error, bad_date} -> error({bad_date, DateStr});
                DateTime -> DateTime
            end;
        DateTime ->
            DateTime
    end.
